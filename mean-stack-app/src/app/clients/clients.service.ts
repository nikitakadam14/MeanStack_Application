import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { HttpWrapperService } from './../app.http-wrapper.service';
import { Client } from 'src/app/clients/client.interface';

@Injectable()
export class ClientsService{
    public clientList: Array<Client> = [];
    public selectedClient: Client = {_id:'',name:'',headquarter:'',noOfEmployees:0,website:'',contactNo:''};

    constructor(private httpwrapperSrv: HttpWrapperService){
    }

    getAllClients() {
        return this.httpwrapperSrv.get('/clients');
    }

    addNewClient(newClient: Client) {
        return this.httpwrapperSrv.post('/clients', newClient);
    }

    deleteClient(clientId: any) {
        return this.httpwrapperSrv.delete('/clients/'+ clientId);
    }

    saveSelectedClient(client: Client) {
        this.selectedClient = client;
    }
    getSelectedClient() {
        return this.selectedClient;
    }
}