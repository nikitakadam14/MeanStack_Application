import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ClientsService } from './clients.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'add-client',
  templateUrl: './addclient.component.html',
  styleUrls: ['./addclient.component.css']
})
export class AddClientComponent implements OnInit {

  public clientForm: FormGroup;
  constructor(private clientService: ClientsService,
              private router: Router) {
  }

  ngOnInit() {
    this.clientForm = new FormGroup ({
      name: new FormControl('', Validators.required),
      headquarter: new FormControl('', Validators.required),
      noOfEmployees: new FormControl('', Validators.required),
      website: new FormControl(),
      contactNo: new FormControl('', Validators.required)
    });
  }

  saveNewClient() {
    if(this.clientForm.valid) {
      this.clientService.addNewClient(this.clientForm.value)
        .subscribe(data => {
          this.router.navigate(['/clients']);
        },
        error => {
        });
    }
  }

  cancel() {
    this.clientForm.reset();
  }

}
