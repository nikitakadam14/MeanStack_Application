import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ClientsService } from './clients.service';
import { Client } from 'src/app/clients/client.interface';

@Component({
  selector: 'clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  public clientList: Array<Client> = [{_id:'',name:'',headquarter:'',noOfEmployees:0,website:'',contactNo:''}];

  constructor(private clientService: ClientsService,
              private router: Router) {
  }

  ngOnInit() {
    this.getAllClients();
  }

  getAllClients() {
    this.clientService.getAllClients()
      .subscribe((data) => {
        this.clientList = data.json();
      },
      (error) => {
      });
  }

  showClientDetails(client: Client) {
    this.clientService.saveSelectedClient(client);
    this.router.navigate(['/employee']);
  }

  deleteClient(event: Event, client: Client) {
    event.stopPropagation();
    this.clientService.deleteClient(client._id)
      .subscribe(data => {
        if(data.ok) {
          this.getAllClients();
        }
      },
      error => {
      });
  }

}
