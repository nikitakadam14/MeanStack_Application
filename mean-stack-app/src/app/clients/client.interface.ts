export interface Client {
    _id: string;
    name: string,
    headquarter: string,
    noOfEmployees: Number,
    website: string,
    contactNo: string
}