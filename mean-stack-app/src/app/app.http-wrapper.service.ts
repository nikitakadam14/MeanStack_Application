import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';

@Injectable() 
export class HttpWrapperService{

    public endpoint: any;

    constructor(private http: Http){
        this.endpoint = environment.endpoint;
    }

    get(url) {
        const newUrl = this.endpoint + url;
        return this.http.get(newUrl);
    }
    post(url, body) {
        const newUrl = this.endpoint + url;
        return this.http.post(newUrl, body);
    }
    delete(url) {
        const newUrl = this.endpoint + url;
        return this.http.delete(newUrl);
    }
}