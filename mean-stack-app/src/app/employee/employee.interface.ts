export interface Employee {
    _id: string,
    name: string,
    address: string,
    experience: Number,
    designation: string,
    noOfTests: Number,
    contactNo: string
}