import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { EmployeeService } from './employee.service';
import { ClientsService } from '../clients/clients.service';
import { Employee } from 'src/app/employee/employee.interface';
import { Client } from 'src/app/clients/client.interface';

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employeeList: Array<Employee> = [{_id:'',name:'',address:'',designation:'',experience:0,noOfTests:0,contactNo:''}];
  public client: Client = {_id:'',name:'',headquarter:'',noOfEmployees:0,website:'',contactNo:''};

  constructor(private router: Router,
                private empService: EmployeeService,
                private clientService: ClientsService) {
  }

  ngOnInit() {
    this.getSelectedClient();
    if (this.client._id === '') {
      this.router.navigate(['/clients']);
    }
    this.getSelectedClientEmployees();
  }

  getSelectedClientEmployees() {
    this.empService.getAllEmployeesByClient(this.client._id)
      .subscribe(data => {
        this.employeeList = data.json();
      },
      error => {
      });
  }

  getSelectedClient() {
    this.client = this.clientService.getSelectedClient();
  }

  showEmployeeDetails(employee: Employee) {
    this.empService.saveSelectedEmployee(employee);
    this.router.navigate(['/tests']);
  }

  deleteEmployee(event: Event, employee: Employee) {
    event.stopPropagation();
    this.empService.deleteEmployee(employee._id)
      .subscribe(data => {
          this.getSelectedClientEmployees();
      })
  }

}
