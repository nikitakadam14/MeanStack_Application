import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { HttpWrapperService } from './../app.http-wrapper.service';
import { Employee } from 'src/app/employee/employee.interface';

@Injectable()
export class EmployeeService{
    public employeeList: Array<Employee> = [];
    public selectedEmployee: Employee = {_id:'',name:'',address:'',designation:'',experience:0,noOfTests:0,contactNo:''};

    constructor(private httpwrapperSrv: HttpWrapperService){
    }

    getAllEmployeesByClient(clientId: string) {
        return this.httpwrapperSrv.get('/employees/' + clientId + '/employee');
    }

    addNewEmployee(clientId: string, newEmployee: Employee) {
        return this.httpwrapperSrv.post('/employees/' + clientId + '/employee', newEmployee)
    }

    saveSelectedEmployee(employee: Employee) {
        this.selectedEmployee = employee;
    }

    getSelectedEmployee() {
        return this.selectedEmployee;
    }

    deleteEmployee(employeeId: any) {
        return this.httpwrapperSrv.delete('/employees/' + employeeId);
    }
}