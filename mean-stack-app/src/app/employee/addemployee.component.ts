import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from './employee.service';
import { ClientsService } from 'src/app/clients/clients.service';
import { Client } from 'src/app/clients/client.interface';

@Component({
  selector: 'add-employee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  public employeeForm: FormGroup;
  public client: Client;

  constructor(private empService: EmployeeService,
              private clientService: ClientsService,
              private router: Router) {
  }

  ngOnInit() {
    this.employeeForm = new FormGroup ({
      name: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      experience: new FormControl('', Validators.required),
      designation: new FormControl('', Validators.required),
      noOfTests: new FormControl(),
      contactNo: new FormControl('', Validators.required)
    });
    this.client = this.clientService.getSelectedClient();
    if(this.client._id === '') {
      this.router.navigate(['/clients']);
    }
  }

  saveNewEmployee() {
    if(this.employeeForm.valid) {
      this.empService.addNewEmployee(this.client._id, this.employeeForm.value)
        .subscribe(data => {
          this.router.navigate(['/employee']);
        },
        error => {
        }); 
    }
  }

  cancel() {
    this.employeeForm.reset();
  }

}
