import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ClientsComponent } from './clients/clients.component';
import { AddClientComponent } from './clients/addclient.component';
import { EmployeeComponent } from './employee/employee.component';
import { AddEmployeeComponent } from './employee/addemployee.component';
import { TestsComponent } from './tests/tests.component';
import { AddTestComponent } from './tests/addtest.component';

import { ClientsService } from './clients/clients.service';
import { HttpWrapperService } from './app.http-wrapper.service';
import { EmployeeService } from './employee/employee.service';
import { TestsService } from './tests/tests.service';

import { routing } from './app.routes';
import { SearchFilterPipe } from './employee/searchfilter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    AddClientComponent,
    EmployeeComponent,
    AddEmployeeComponent,
    TestsComponent,
    AddTestComponent,
    SearchFilterPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [
    HttpWrapperService,
    ClientsService,
    EmployeeService,
    TestsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
