export interface Test {
    _id: string,
    testCode: string,
    name: string,
    result: string
}