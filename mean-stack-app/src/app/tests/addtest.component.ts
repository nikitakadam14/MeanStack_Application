import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TestsService } from './tests.service';
import { EmployeeService } from 'src/app/employee/employee.service';
import { Employee } from 'src/app/employee/employee.interface';

@Component({
  selector: 'add-test',
  templateUrl: './addtest.component.html',
  styleUrls: ['./addtest.component.css']
})
export class AddTestComponent implements OnInit {

  public testResultForm: FormGroup;
  public employee: Employee;
  
  constructor(private testService: TestsService,
              private empService: EmployeeService,
              private router: Router) {
  }

  ngOnInit() {
    this.testResultForm = new FormGroup ({
      testCode: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      result: new FormControl('', Validators.required)
    });
    this.employee = this.empService.getSelectedEmployee();
    if(this.employee._id === '') {
      this.router.navigate(['/tests']);
    }
  }

  saveNewTest() {
    if(this.testResultForm.valid) {
      this.testService.createNewTest(this.testResultForm.value, this.employee._id)
        .subscribe(data => {
          this.router.navigate(['/tests'])
        },
        error => {
        });
    }
  }

  cancel() {
    this.testResultForm.reset();
  }

}
