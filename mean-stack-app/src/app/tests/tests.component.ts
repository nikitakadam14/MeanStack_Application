import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TestsService } from './tests.service';
import { EmployeeService } from '../employee/employee.service';
import { Employee } from 'src/app/employee/employee.interface';
import { Test } from 'src/app/tests/test.interface';

@Component({
  selector: 'tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.css']
})
export class TestsComponent implements OnInit {

  public testsList: Array<Test> = [{_id:'', testCode:'', name:'', result:''}];
  public employee: Employee = {_id:'', name:'', address:'', designation:'', experience:0, noOfTests:0, contactNo:''};

  constructor(private router: Router,
                private testsService: TestsService,
                private empService: EmployeeService) {
  }

  ngOnInit() {
    this.getSelectedEmployee();
    if (this.employee._id === '') {
      this.router.navigate(['/clients']);
    }
    this.getSelectedTestByEmployee();
  }

  getSelectedTestByEmployee() {
    this.testsService.getAllTestByEmployee(this.employee._id)
      .subscribe(data => {
        this.testsList = data.json();
      },
      error => {
      });
  }

  getSelectedEmployee() {
    this.employee = this.empService.getSelectedEmployee();
  }

  deleteTest(event:Event, test: Test) {
    event.stopPropagation();
    this.testsService.deleteTest(test._id)
      .subscribe(data => {
        this.getSelectedTestByEmployee();
      },
      error => {
      });
  }

}
