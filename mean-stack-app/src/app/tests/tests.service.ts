import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { HttpWrapperService } from './../app.http-wrapper.service';
import { Test } from 'src/app/tests/test.interface';

@Injectable()
export class TestsService{

    constructor(private httpwrapperSrv: HttpWrapperService){
    }

    getAllTestByEmployee(empId: string) {
        return this.httpwrapperSrv.get('/tests/' + empId + '/test')
    }

    createNewTest(newTest: Test, empId: string) {
        return this.httpwrapperSrv.post('/tests/' + empId + '/test', newTest);
    }

    deleteTest(empId: string) {
        return this.httpwrapperSrv.delete('/tests/' + empId);
    }
}