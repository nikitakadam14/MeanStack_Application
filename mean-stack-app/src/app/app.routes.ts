import { AddEmployeeComponent } from './employee/addemployee.component';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from "./clients/clients.component";
import { ModuleWithProviders } from "@angular/core";
import { AddClientComponent } from './clients/addclient.component';
import { EmployeeComponent } from './employee/employee.component';
import { TestsComponent } from './tests/tests.component';
import { AddTestComponent } from './tests/addtest.component';

export const routes: Routes = [
    { path: 'clients', component: ClientsComponent },
    { path: 'addclient', component: AddClientComponent },
    { path: 'employee', component: EmployeeComponent },
    { path: 'addemployee', component: AddEmployeeComponent},
    { path: 'tests', component: TestsComponent},
    { path: 'addtest', component: AddTestComponent},
    { path: '', redirectTo: '/clients', pathMatch: 'full'}
  ];
   
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);