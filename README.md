Steps to Install and Run:

1.Start mongodb
	database name -> meanstack
	collections -> clients, employees, tests

	1.1 Run "mongod" from bin location of mongodb files.
		for eg. "C:\Program Files\MongoDB\Server\3.2\bin"
		(make sure you have data\db folder at C:/ location).

	1.2 Import the Json files from the data folder using following command:

		mongoimport --db meanstack --collection clients --file <clients.json path>
		mongoimport --db meanstack --collection employees --file <employees.json path>
		mongoimport --db meanstack --collection tests --file <tests.json path>

		Run these commands from the following location:
			C:\Program Files\MongoDB\Server\3.2\bin

		Note:- If the collections are already created use "--upsert"
			For eg: mongoimport --db meanstack --collection clients --file <clients.json path> --upsert

	

2.Start Node (back end folder).
	2.1 Run "npm install" at the root of the node folder.

	2.2 Once the packages are installed run
			"npm start" at the same location.

	2.3 Your web services will start running on http://localhost:3000

	Note:- If there is nodemon.js error after running "npm start" command,
				edit some line of code to restart the node server and it will start the server again.

3.Mean-stack-app ( Front end code).
	3.1 Run "npm install" at root of this folder.(It will take some time to load the packages)

	3.2 Run "ng serve" at the same path.

	3.3 Your application will start working on http://localhost:4200 (default port)

	Note:- If this port is already in use then specify some different port using following command
			"ng serve --port 4400"
