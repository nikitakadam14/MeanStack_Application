const Employee = require('../models/employee');
const Test = require('../models/test');

module.exports = {

    index: async(req, res, next) => {
        const tests = await Test.find({});
        res.status(200).json(tests);
    },

    getTestById: async (req, res, next) => {
        const { testId } = req.params;
        const test = await Test.findById(testId);
        res.status(200).json(test);
    },

    deleteTest: async (req, res, next) => {
        const { testId } = req.params;

        const test = await Test.findById(testId);
        const empId = test.employee[0];

        await Test.remove({ _id: testId});
        await Employee.update({"_id": empId}, {'$pull': {"test": testId}});
        res.status(200).json({message: 'successfully deleted'});
    },


    getEmployeesTest: async (req, res, next) => {
        const { empId } = req.params;
        const employee = await Employee.findById(empId).populate('test');
        res.status(201).json(employee.test);
    },

    newEmployeesTest: async (req, res, next) => {
        const newtest = new Test(req.body);
        const { empId } = req.params;
        const employee = await Employee.findById(empId);
        newtest.employee = employee;
        await newtest.save();

        employee.test.push(newtest);
    
        await employee.save();
        res.status(201).json(newtest);
     }

};
