const Client = require('../models/client');

module.exports = {
    index: async(req, res, next) => {
        const clients = await Client.find({});
        res.status(200).json(clients);
    },

    newClient: async (req, res, next) => {
        const newClient = new Client(req.body);
        const client = await newClient.save();
        res.status(201).json(client);
    },

    getClientById: async (req, res, next) => {
        const { clientId } = req.params;
        const client = await Client.findById(clientId);
        res.status(200).json(client);
    },

    updateClient: async (req, res, next) => {
        const { clientId } = req.params;
        const newClient = req.body;
        const result = await Client.findByIdAndUpdate(clientId, newClient);
        res.status(200).json({ success: true });
    },

    deleteClient: async (req, res, next) => {
        const { clientId } = req.params;
        const client = await Client.remove({ _id: clientId});
        res.status(200).json(client);
    }

};