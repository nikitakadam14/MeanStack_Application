const Client = require('../models/client');
const Employee = require('../models/employee');

module.exports = {

    index: async(req, res, next) => {
        const employees = await Employee.find({});
        res.status(200).json(employees);
    },

    getEmpById: async (req, res, next) => {
        const { empId } = req.params;
        const employee = await Employee.findById(empId);
        res.status(200).json(employee);
    },

    // updateEmployee: async (req, res, next) => {
    //     const { empId } = req.params;
    //     const newEmp = req.body;
    //     const result = await Employee.findByIdAndUpdate(empId, newEmp);
    //     res.status(200).json({ success: true });
    // },

    deleteEmployee: async (req, res, next) => {
        const { empId } = req.params;

        const employee = await Employee.findById(empId);
        const clientId = employee.client[0];

        await Employee.remove({ _id: empId});
        await Client.update({"_id": clientId}, {'$pull': {"employee": empId}});
        res.status(200).json({message: 'successfully deleted'});
    },


    getClientsEmployee: async (req, res, next) => {
        const {clientId} = req.params;
        const client = await Client.findById(clientId).populate('employee');
        res.status(201).json(client.employee);
    },

    newClientEmployee: async (req, res, next) => {
        const newemployee = new Employee(req.body);
        const { clientId } = req.params;
        const client = await Client.findById(clientId);
        newemployee.client = client;
        await newemployee.save();
        client.employee.push(newemployee);
    
        await client.save();
        res.status(201).json(newemployee);
     }

};
