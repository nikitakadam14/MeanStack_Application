const express = require("express");
const router = express.Router();

const EmployeeController = require("../controllers/employees");

// get all employees
router.route('/')
  .get(EmployeeController.index)

// get -> employee by id
// delete -> employee by id
router.route('/:empId')
  .get(EmployeeController.getEmpById)
  .delete(EmployeeController.deleteEmployee);

// get -> get all employees for supplied client id
// post -> create new employee for supplied client id
router.route('/:clientId/employee')
  .get(EmployeeController.getClientsEmployee)
  .post(EmployeeController.newClientEmployee)

module.exports = router;



