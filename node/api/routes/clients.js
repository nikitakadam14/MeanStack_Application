const express = require("express");
const router = express.Router();

const ClientsController = require('../controllers/clients');

// get all clients
// create a new client
router.route('/')
  .get(ClientsController.index)
  .post(ClientsController.newClient);

// get client by id
// update client by id
// delete client by id
router.route('/:clientId')
  .get(ClientsController.getClientById)
  .put(ClientsController.updateClient)
  .delete(ClientsController.deleteClient);

module.exports = router;
