const express = require("express");
const router = express.Router();

const TestsController = require("../controllers/tests");

// get all tests
router.route('/')
  .get(TestsController.index);

// get -> get test by id
// delete -> delete test by id
router.route('/:testId')
  .get(TestsController.getTestById)
  .delete(TestsController.deleteTest);

// get -> get all tests for supplied employee id
// post -> create new test for supplied employee id
router.route('/:empId/test')
  .get(TestsController.getEmployeesTest)
  .post(TestsController.newEmployeesTest);

module.exports = router;
