const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    name: String,
    headquarter: String,
    noOfEmployees: Number,
    website: String,
    contactNo: String,
    employee: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }]
});

module.exports = mongoose.model('Client', clientSchema);
