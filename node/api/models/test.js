const mongoose = require('mongoose');

const testSchema = mongoose.Schema({
    testCode: String,
    name: String,
    createdOn: Date,
    result: String,
    employee: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }]
});

module.exports = mongoose.model('Test', testSchema);
