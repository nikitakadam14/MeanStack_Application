const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    name: String,
    address: String,
    experience: Number,
    designation: String,
    noOfTests: Number,
    contactNo: String,
    client: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    }],
    test: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Test'
    }]
});

module.exports = mongoose.model('Employee', employeeSchema);
